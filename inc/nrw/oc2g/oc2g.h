/****************************************************************************
 *        _  ____  _____  ___   _  __  _      ___         __
 *       / |/ / / / / _ \/ _ | / |/ / | | /| / (_)______ / /__ ___ ___
 *      /    / /_/ / , _/ __ |/    /  | |/ |/ / / __/ -_) / -_|_-<(_-<
 *     /_/|_/\____/_/|_/_/ |_/_/|_/   |__/|__/_/_/  \__/_/\__/___/___/
 *
 ************************************************************************//**
 *
 * @file       oc2g.h
 * @brief      OC-2G system primitive definitions
 * @author     Yves Godin
 *
 * Copyright 2018, NuRAN Wireless Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/
#ifndef OC2G_H__
#define OC2G_H__

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus 

/****************************************************************************
 *                                  Includes                                *
 ****************************************************************************/
#include <stdint.h>

#include "gsml1const.h"


/****************************************************************************
 *                                   Const                                  *
 ****************************************************************************/

/**
 * API Version
 */
#define OC2G_API_VERSION_MAJOR    2
#define OC2G_API_VERSION_MINOR    1
#define OC2G_API_VERSION_BUILD    6

#define OC2G_API(x,y,z)    ((x << 16) | (y << 8) | z)
#define OC2G_API_VERSION   OC2G_API(OC2G_API_VERSION_MAJOR,OC2G_API_VERSION_MINOR,OC2G_API_VERSION_BUILD)

 
/****************************************************************************
 * Enum : Oc2g_PrimId_t
 ************************************************************************//**
 *   
 *   System management primitives.
 *
 *   @ingroup oc2g_api
 *   
 ****************************************************************************/
typedef enum Oc2g_PrimId_t
{ 
    Oc2g_PrimId_SystemInfoReq = 0x100,          ///< REQ: System information
    Oc2g_PrimId_SystemInfoCnf,                  ///< CNF: System information
    Oc2g_PrimId_SystemFailureInd,               ///< IND: System failure indication
    Oc2g_PrimId_ActivateRfReq,                  ///< REQ: Activation of the RF section
    Oc2g_PrimId_ActivateRfCnf,                  ///< CNF: Activation of the RF section
    Oc2g_PrimId_DeactivateRfReq,                ///< REQ: Deactivation of the RF section
    Oc2g_PrimId_DeactivateRfCnf,                ///< CNF: Deactivation of the RF section
    Oc2g_PrimId_SetTraceFlagsReq,               ///< REQ: Set the verbosity of the system
    Oc2g_PrimId_Layer1ResetReq,                 ///< REQ: Trigger a reset of the GSM layer 1 library
    Oc2g_PrimId_Layer1ResetCnf,                 ///< CNF: Confirm that the GSM layer 1 library was reset
    Oc2g_PrimId_SetCalibTblReq,                 ///< REQ: Update a calibration table (may be partial)
    Oc2g_PrimId_SetCalibTblCnf,                 ///< CNF: Confirm the update of a calibration table
    Oc2g_PrimId_MuteRfReq,                      ///< REQ: Mute/Unmute the RF section
    Oc2g_PrimId_MuteRfCnf,                      ///< CNF: Confirm the mutin/unmiting of the the RF section
    Oc2g_PrimId_SetRxAttenReq,                  ///< REQ: Set the RX attenuation
    Oc2g_PrimId_SetRxAttenCnf,                  ///< CNF: Confirm the configuration of the RX attenuation
    Oc2g_PrimId_IsAliveReq,                     ///< REQ: Is alive validation
    Oc2g_PrimId_IsAliveCnf,                     ///< CNF: Confirm of is alive validation
    Oc2g_PrimId_SetMaxCellSizeReq,              ///< REQ: Set the maximum cell size
    Oc2g_PrimId_SetMaxCellSizeCnf,              ///< CNF: Confirm the configuration of the maximum cell size
    Oc2g_PrimId_SetC0IdleSlotPowerReductionReq, ///< REQ: Set the C0 power reduction for idle timeslots
    Oc2g_PrimId_SetC0IdleSlotPowerReductionCnf, ///< CNF: Confirm the configuration of the C0 power reduction

    Oc2g_PrimId_NUM = 22

} Oc2g_PrimId_t;

/****************************************************************************
 *                                  Types                                   *
 ****************************************************************************/

/****************************************************************************
 * Enum: Oc2g_Status_t
 ************************************************************************//**
 *
 * Status used by the OC2G.
 *
 * @ingroup oc2g_api
 *
 ****************************************************************************/
typedef GsmL1_Status_t Oc2g_Status_t;

/****************************************************************************
 * Struct : Oc2g_SystemInfoCnf_t
 ************************************************************************//**
 * 
 * Structure is used to confirm the information about the system.
 * 
 * @ingroup oc2g_api_prim_sys
 *
****************************************************************************/
typedef struct Oc2g_SystemInfoCnf
{
    Oc2g_Status_t status;       ///< Status
   
    struct
    {
        uint8_t major;              ///< DSP firmware major version number 
        uint8_t minor;              ///< DSP firmware minor version number
        uint8_t build;              ///< DSP firmware build version number
    } dspVersion;

    struct
    {
        uint8_t major;              ///< FPGA firmware major version number 
        uint8_t minor;              ///< FPGA firmware minor version number
        uint8_t build;              ///< FPGA firmware build version number
    } fpgaVersion;
    
    struct
    {
        uint8_t rev;                ///< Board reversion number 
        uint8_t model;              ///< Board option number
    } boardVersion;
    
} Oc2g_SystemInfoCnf_t;
 
/****************************************************************************
 * Struct : Oc2g_SystemFailureInd_t
 ************************************************************************//**
 * 
 * Structure is used to indicate a system failure.
 * 
 * @ingroup oc2g_api_prim_sys
 *
****************************************************************************/
typedef struct Oc2g_SystemFailureInd
{
   Oc2g_Status_t status;          ///< Status

} Oc2g_SystemFailureInd_t;

/****************************************************************************
 * Struct : Oc2g_ActivateRfReq_t
 ************************************************************************//**
 *
 * This primitive is used to request the activation the RF section of the system.
 *
 * @ingroup oc2g_api
 *
 ***************************************************************************/
typedef struct Oc2g_ActivateRfReq
{
    uint8_t u8MaxCellSize;          ///< Maximum cell size in qbits (1 qbit = 138.4 meters, max 166 qbits)
    uint8_t u8UnusedTsMode;         ///< Unused timeslot mode (0:off, 1:min TX power)

    struct {
        uint8_t u8UseTchMsgq;       ///< Set to '1' to use a separate MSGQUEUE for TCH primitives
        uint8_t u8UsePdtchMsgq;     ///< Set to '1' to use a separate MSGQUEUE for PDTCH primitives
    } msgq;

    uint8_t u8EnAutoPowerAdjust;    ///< Automatic output power adjustment (0:Disable, 1:Enable)

} Oc2g_ActivateRfReq_t;

/****************************************************************************
 * Struct : Oc2g_ActivateRfCnf_t
 ************************************************************************//**
 *
 * This primitive is used to confirm the activation the RF section of the system.
 *
 * @ingroup oc2g_api
 *
 ***************************************************************************/
typedef struct Oc2g_ActivateRfCnf
{
     Oc2g_Status_t status;      ///< Status
    
} Oc2g_ActivateRfCnf_t;

/****************************************************************************
 * Struct : Oc2g_DeactivateRfCnf_t
 ************************************************************************//**
 *
 * This primitive is used to confirm the deactivation the RF section of the system.
 *
 * @ingroup oc2g_api
 *
 ***************************************************************************/
typedef struct Oc2g_DeactivateRfCnf
{
     Oc2g_Status_t status;      ///< Status
    
} Oc2g_DeactivateRfCnf_t;

/****************************************************************************
 * Struct : Oc2g_SetTraceFlagsReq_t
 ************************************************************************//**
 *
 * This primitive is used to setup the trace flag values. 
 *
 * @ingroup oc2g_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Oc2g_SetTraceFlagsReq
{
    uint32_t u32Tf;                 ///< Trace flag level 
} Oc2g_SetTraceFlagsReq_t;


/****************************************************************************
 * Struct : Oc2g_Layer1ResetReq_t
 ************************************************************************//**
 *
 * Structure is used to request a reset of the layer 1 module.
 *
 * @ingroup oc2g_api_prim_sys
 *
 ****************************************************************************/
typedef struct Oc2g_Layer1ResetReq
{
    uint32_t u32Reserved;

} Oc2g_Layer1ResetReq_t;

/****************************************************************************
 * Struct : Oc2g_Layer1ResetCnf_t
 ************************************************************************//**
 * 
 * Structure is used to confirm the reset of the layer 1 module.
 * 
 * @ingroup oc2g_api_prim_sys
 *
 ****************************************************************************/
typedef struct Oc2g_Layer1ResetCnf
{
    GsmL1_Status_t status;

} Oc2g_Layer1ResetCnf_t;

/****************************************************************************
 * Struct : Oc2g_IsAliveCnf_t
 ************************************************************************//**
 * 
 * Structure is used to confirm the is alive.
 * 
 * @ingroup oc2g_api_prim_sys
 *
 ****************************************************************************/
typedef struct Oc2g_IsAliveCnf
{
    GsmL1_Status_t status;

} Oc2g_IsAliveCnf_t;

/****************************************************************************
 * Struct : Oc2g_SetCalibTblReq_t
 ************************************************************************//**
 *
 * Structure is used to update a calibration table (may be partial).
 *
 * @ingroup oc2g_api_prim_sys
 *
 ****************************************************************************/
typedef struct Oc2g_SetCalibTblReq
{
    uint32_t offset;                ///< Offset of the data
    uint32_t length;                ///< Length of this chunk in bytes
    uint8_t u8Data[1024];           ///< Chuck of the calibration table
} Oc2g_SetCalibTblReq_t;

/****************************************************************************
 * Struct : Oc2g_SetCalibTblCnf_t
 ************************************************************************//**
 *
 * Structure is used to confirm the update of a calibration table
 *
 * @ingroup oc2g_api_prim_sys
 *
 ****************************************************************************/
typedef struct Oc2g_SetCalibTblCnf
{
    uint32_t       offset;          ///< Offset of the data
    uint32_t       length;          ///< Length of this chunk in bytes
    GsmL1_Status_t status;
} Oc2g_SetCalibTblCnf_t;

/****************************************************************************
 * Struct : Oc2g_MuteRfReq_t
 ************************************************************************//**
 *
 * This primitive is used configure if the RF signal should be muted or not
 * for each of the 8 timeslots.
 *
 * @ingroup oc2g_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Oc2g_MuteRfReq
{       
   uint8_t u8Mute[8];            ///< Timeslot mute flag (0:unmute, 1:mute)

} Oc2g_MuteRfReq_t;

/****************************************************************************
 * Struct : Oc2g_MuteRfCnf_t
 ************************************************************************//**
 *
 * This primitive is sent back to confirm the configuration RF mute.
 *
 * @ingroup oc2g_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Oc2g_MuteRfCnf
{        
     Oc2g_Status_t status;   ///< Status of the MUTE-RF-REQ

} Oc2g_MuteRfCnf_t;

/****************************************************************************
 * Struct : Oc2g_SetRxAttenReq_t
 ************************************************************************//**
 *
 * This primitive is used to configure the RF receive attenuation.
 *
 * @ingroup oc2g_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Oc2g_SetRxAttenReq
{
   uint8_t u8Atten;                ///< RX Attenuation: 0(default), 12, 24, 36 dB

} Oc2g_SetRxAttenReq_t;

/****************************************************************************
 * Struct : Oc2g_SetRxAttenCnf_t
 ************************************************************************//**
 *
 * This primitive is sent back to confirm the configuration of the RF receive
 * attenuation.
 *
 * @ingroup oc2g_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Oc2g_SetRxAttenCnf
{
     Oc2g_Status_t status;   ///< Status of the SET-RX-ATTEN-REQ

} Oc2g_SetRxAttenCnf_t;

/****************************************************************************
 * Struct : Oc2g_SetMaxCellSizeReq_t
 ************************************************************************//**
 *
 * This primitive is used to configure the maximum cell size.
 *
 * @ingroup oc2g_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Oc2g_SetMaxCellSizeReq
{
    uint8_t u8MaxCellSize;          ///< Maximum cell size in qbits (1 qbit = 138.4 meters, max 166 qbits)

} Oc2g_SetMaxCellSizeReq_t;

/****************************************************************************
 * Struct : Oc2g_SetMaxCellSizeCnf_t
 ************************************************************************//**
 *
 * This primitive is sent back to confirm the configuration of themaximum
 * cell size.
 *
 * @ingroup oc2g_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Oc2g_SetMaxCellSizeCnf
{
     Oc2g_Status_t status;   ///< Status of the SET-MAXCELLSIZE-REQ

} Oc2g_SetMaxCellSizeCnf_t;

/****************************************************************************
 * Struct : Oc2g_SetC0IdleSlotPowerReductionReq_t
 ************************************************************************//**
 *
 * This primitive is used to configure the C0 idle slot power reduction
 *
 * @ingroup oc2g_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Oc2g_SetC0IdleSlotPowerReductionReq
{
    uint8_t u8PowerReduction;   ///< C0 idle slot power reduction

} Oc2g_SetC0IdleSlotPowerReductionReq_t;

/****************************************************************************
 * Struct : Oc2g_SetC0IdleSlotPowerReductionCnf_t
 ************************************************************************//**
 *
 * This primitive is sent back to confirm the configuration of the
 * C0 idle slot power reduction.
 *
 * @ingroup oc2g_api_prim_dbg
 *
 ***************************************************************************/
typedef struct Oc2g_SetC0IdleSlotPowerReductionCnf
{
     Oc2g_Status_t status;   ///< Status of the SET-C0-IDLE-SLOT-POWER-REDUCTION-REQ

} Oc2g_SetC0IdleSlotPowerReductionCnf_t;

/****************************************************************************
 * Struct : Oc2g_Primt_t
 ************************************************************************//**
 * 
 * System level primitive definition.
 * 
 * @ingroup oc2g_api_prim_sys
 *
 ****************************************************************************/
typedef struct Oc2g_Prim
{
    Oc2g_PrimId_t id;                               ///< Primitive ID

    union 
    {     
        Oc2g_SystemInfoCnf_t                  systemInfoCnf;                  ///< CNF: System information
        Oc2g_SystemFailureInd_t               systemFailureInd;               ///< IND: System failure indication
        Oc2g_ActivateRfReq_t                  activateRfReq;                  ///< REQ: Activation of the RF section
        Oc2g_ActivateRfCnf_t                  activateRfCnf;                  ///< CNF: Activation of the RF section
        Oc2g_DeactivateRfCnf_t                deactivateRfCnf;                ///< CNF: Deactivation of the RF section
        Oc2g_SetTraceFlagsReq_t               setTraceFlagsReq;               ///< REQ: Set the verbosity of the system
        Oc2g_Layer1ResetReq_t                 layer1ResetReq;                 ///< REQ: Trigger a reset of the GSM layer 1 module
        Oc2g_Layer1ResetCnf_t                 layer1ResetCnf;                 ///< CNF: Confirm that the GSM layer 1 module was reset
        Oc2g_SetCalibTblReq_t                 setCalibTblReq;                 ///< REQ: Update a calibration table (may be partial)
        Oc2g_SetCalibTblCnf_t                 setCalibTblCnf;                 ///< CNF: Confirm the update a calibration table (may be partial)
        Oc2g_MuteRfReq_t                      muteRfReq;                      ///< REQ: Mute/Unmute the RF section
        Oc2g_MuteRfCnf_t                      muteRfCnf;                      ///< CNF: Confirm the mutin/unmiting of the the RF section
        Oc2g_SetRxAttenReq_t                  setRxAttenReq;                  ///< REQ: Set the RX attenuation
        Oc2g_SetRxAttenCnf_t                  setRxAttenCnf;                  ///< CNF: Confirm the configuration of the RX attenuation
        Oc2g_IsAliveCnf_t                     isAliveCnf;                     ///< CNF: Confirm that the module is alive
        Oc2g_SetMaxCellSizeReq_t              setMaxCellSizeReq;              ///< REQ: Set the maximum cell size
        Oc2g_SetMaxCellSizeCnf_t              setMaxCellSizeCnf;              ///< CNF: Confirm the maximum cell size
        Oc2g_SetC0IdleSlotPowerReductionReq_t setC0IdleSlotPowerReductionReq; ///< REQ: Set C0 idle slot power reduction
        Oc2g_SetC0IdleSlotPowerReductionCnf_t setC0IdleSlotPowerReductionCnf; ///< CNF: Confirm C0 idle slot power reduction
    } u;

} Oc2g_Prim_t;

 
#ifdef _TMS320C6600
 
/****************************************************************************
 *                               Public Functions                           * 
 ****************************************************************************/

/****************************************************************************
 * Function : Oc2g_RxPrimCallback
 ************************************************************************//**
 * 
 * Oc2g primitive reception callback routine. Process primitives sent
 * by the ARM processor.
 *
 * @param [in] pPrim
 *      Address of the buffer containing the received primitive.
 *
 * @return
 *      GsmL1_Status_Success or the error code
 *  
 * @ingroup oc2g_api_prim_sys
 *
 ****************************************************************************/ 
void Oc2g_RxPrimCallback( Oc2g_Prim_t *pPrim );
 
 
#endif  // _TMS320C6600

#ifdef __cplusplus
}
#endif  // extern "C"

#endif  // OC2G_H__
