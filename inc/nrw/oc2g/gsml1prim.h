/****************************************************************************
 *        _  ____  _____  ___   _  __  _      ___         __
 *       / |/ / / / / _ \/ _ | / |/ / | | /| / (_)______ / /__ ___ ___
 *      /    / /_/ / , _/ __ |/    /  | |/ |/ / / __/ -_) / -_|_-<(_-<
 *     /_/|_/\____/_/|_/_/ |_/_/|_/   |__/|__/_/_/  \__/_/\__/___/___/
 *
 ************************************************************************//**
 *
 * @file       gsml1prim.h
 * @brief      GSM Layer 1 API primitives types definitions.
 * @author     Yves Godin
 *
 * Copyright 2006, NuRAN Wireless Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ****************************************************************************/
#ifndef GSML1PRIM_H__
#define GSML1PRIM_H__

#ifdef __cplusplus
    extern "C" {
#endif // __cplusplus 

/****************************************************************************
 *                                 Includes                                 *
 ****************************************************************************/

#ifndef WIN32
    #include <stdint.h>
#endif

#include "gsml1const.h"
#include "gsml1types.h"

/****************************************************************************
 *                            Types (Primitive Parameters)                  *
 ****************************************************************************/
                     
/****************************************************************************
 * typedef : GsmL1_MsgUnitParam_t
 ************************************************************************//**
 *
 * Buffer structure used to transfer all messages frame units between 
 * the layers. 
 *   
 * @ingroup gsml1_api_prim_params
 *
 ***************************************************************************/
typedef struct GsmL1_MsgUnitParam_t
{            
    uint8_t u8Buffer[MAX_L2MSGSIZE];   ///< Buffer containing the msg. unit
    uint8_t u8Size;                    ///< Size of the msg. unit
} GsmL1_MsgUnitParam_t;

/****************************************************************************
 * typedef : GsmL1_MeasParam_t
 ************************************************************************//**
 *
 * Used to report library specific measured informations.
 *    
 * @ingroup gsml1_api_prim_params
 *  
 ***************************************************************************/
typedef struct GsmL1_MeasParam_t
{
    float    fRssi;          ///< Received Signal Strength Indication (in dBm)    
    int16_t  i16BurstTiming; ///< Burst timing (in quarter of bits)    
    float    fLinkQuality;   ///< Link quality of the communication (dB)  

    float    fBer;           ///< Bit error rate of the last decoded frame [0..1]

} GsmL1_MeasParam_t;

/****************************************************************************
 * typedef : GsmL1_DeviceParam_t
 ************************************************************************//**
 *
 * TRX device parameters
 *    
 * @ingroup gsml1_api_prim_params
 *
 ***************************************************************************/
typedef struct GsmL1_DeviceParam_t
{
    GsmL1_DevType_t devType;    ///< Device type
    
    GsmL1_FreqBand_t freqBand;  ///< GSM Frequency band used by the TRX device
    uint16_t u16Arfcn;          ///< Radio frequency channel number
    uint16_t u16BcchArfcn;      ///< Associated beacon radio frequency channel number    
    
    float    fRxPowerLevel;     ///< Nominal RX power level in dBm (use 0 if the control is handled by upper layers)
    float    fTxPowerLevel;     /**< Transmission power level if applicable (in dBm).
                                     @see Verify the values supported by the hardware. */

    uint8_t  u8NbTsc;           ///< Training sequence code to be used for normal bursts   
    uint8_t  u8AutoTA;          ///< Automatic timing advance (0:TA is handled by upper layers, 1:TA is handled by layer 1)
} GsmL1_DeviceParam_t;


/****************************************************************************
 *                               Types (Primitives)                         *
 ****************************************************************************/
 
/****************************************************************************
 * Struct : GsmL1_MphInitReq_t 
 ************************************************************************//**
 *
 * This structure contains every necessary parameters to initialize a 
 * Base Tranceiver Station on a specific GSM Radio Frequency band.
 *                                                               
 * Primitive : MPH-INIT-REQ (L3 -> L1)
 * 
 * @ingroup gsml1_api_prim_mph   
 *    
 ****************************************************************************/
typedef struct GsmL1_MphInitReq_t
{
    HANDLE hLayer3;                     ///< Handle to a layer 3 entity
    GsmL1_DeviceParam_t deviceParam;    ///< TRX Device parameters

} GsmL1_MphInitReq_t;

/***************************************************************************
 * Struct : GsmL1_MphInitCnf
 ************************************************************************//**
 *
 * This structure is sent back to the layer 3 in order to confirm the 
 * reception of a MPH-INIT-REQ primitive.
 *   
 * @ingroup gsml1_api_prim_mph
 *
 ***************************************************************************/                                 
typedef struct GsmL1_MphInitCnf_t
{       
    HANDLE           hLayer3;  /**< Handle to the layer 3 entity.
    
                                    The layer 3 handle received in the 
                                    MPH-INIT-REQ primitive is sent back 
                                    to the layer 3 (unmodified). */
                                      
    HANDLE           hLayer1;  ///< Handle to the initialized Layer1 entity    
    GsmL1_FreqBand_t freqBand; ///< GSM Frequency band used by the TRX device
    
    GsmL1_Status_t   status;   ///< Status of the MPH-INIT-REQ primitive
    
} GsmL1_MphInitCnf_t;

/***************************************************************************
 * Struct : GsmL1_MphCloseReq
 ************************************************************************//**
 *
 * This structure contains every necessary parameters to close a 
 * Base Tranceiver Station using a specific GSM Radio Frequency band.
 *   
 * @ingroup gsml1_api_prim_mph
 *
 ***************************************************************************/                                 
typedef struct GsmL1_MphCloseReq_t
{       
    HANDLE hLayer3; ///< Handle to the layer 3 entity
    HANDLE hLayer1; ///< Handle of the Layer1 entity to close
           
} GsmL1_MphCloseReq_t;   

/***************************************************************************
 * Struct : GsmL1_MphCloseCnf
 ************************************************************************//**
 *
 * This structure is sent back to the layer 3 in order to confirm the 
 * reception of a MPH-CLOSE-REQ primitive.
 *   
 * @ingroup gsml1_api_prim_mph
 *
 ***************************************************************************/                                 
typedef struct GsmL1_MphCloseCnf_t
{           
    HANDLE hLayer3;        ///< Handle to the layer 3 entity

    GsmL1_Status_t status; ///< Status of the MPH-CLOSE-REQ primitive
    
} GsmL1_MphCloseCnf_t;   

/****************************************************************************
 * Struct : GsmL1_MphConnectReq_t 
 ************************************************************************//**
 *
 * This structure contains all the necessary parameters to connect a physical 
 * channel on an layer 1 entity.                                                               
 *
 * Primitive : MPH-CONNECT-REQ (L3 -> L1)
 * 
 * @ingroup gsml1_api_prim_mph   
 *    
 ****************************************************************************/ 
typedef struct GsmL1_MphConnectReq_t
{
    HANDLE             hLayer3;     ///< Handle to a layer 3 entity    
    HANDLE             hLayer1;     /**< Handle to the layer1 on which the 
                                         physical channel is connected. */

    uint8_t            u8Tn;        ///< Time-slot [0..7]   
    GsmL1_LogChComb_t  logChComb;   /**< Logical channel multiplexing 
                                         over the physical channel */
                                                                                                                    
} GsmL1_MphConnectReq_t;
  
/***************************************************************************
 * Struct : GsmL1_MphConnectCnf 
 ************************************************************************//**
 *
 * This structure is sent back to the layer 3 in order to confirm the 
 * reception of a MPH-CONNECT-REQ primitive.
 *   
 * @ingroup gsml1_api_prim_mph
 *
 ***************************************************************************/                                 
typedef struct GsmL1_MphConnectCnf_t
{       
    HANDLE         hLayer3;  /**< The layer 3 handle received from in 
                                  MPH-CONNECT-REQ primitive is sent back to 
                                  the layer 3 (unmodified). */
    HANDLE         hLayer1;   /**< Handle to the layer1 on which the 
                                   physical channel is connected. */

    GsmL1_Status_t status;   ///< Status of the MPH-INIT-CONNECT-REQ primitive

    uint8_t        u8Tn;     ///< Time-slot [0..7]    
    
} GsmL1_MphConnectCnf_t;   

/***************************************************************************
 * Struct : GsmL1_MphDisconnectReq_t
 ************************************************************************//**
 *
 * This structure contains all the necessary parameters to disconnect a 
 * physical channel on an layer 1 entity.     
 *   
 * @ingroup gsml1_api_prim_mph
 *
 ***************************************************************************/                                 
typedef struct GsmL1_MphDisconnectReq_t
{           
    HANDLE         hLayer3;  ///< Handle to a layer 3 entity
    HANDLE         hLayer1;  /**< Handle to the layer1 on which the 
                                  physical channel is disconnected. */

    uint8_t        u8Tn;     ///< Time-slot [0..7]

} GsmL1_MphDisconnectReq_t;   

/***************************************************************************
 * Struct : GsmL1_MphDisconnectCnf
 ************************************************************************//**
 *
 * This structure is sent back to the layer 3 in order to confirm the 
 * reception of a MPH-DISCONNECT-REQ primitive.
 *   
 * @ingroup gsml1_api_prim_mph
 *
 ***************************************************************************/                                 
typedef struct GsmL1_MphDisconnectCnf_t
{       
    HANDLE         hLayer3;  /**< The layer 3 handle received from in 
                                  MPH-DISCONNECT-REQ primitive is sent 
                                  back to the layer 3 (unmodified). */
    HANDLE         hLayer1;  /**< Handle to the layer1 on which the 
                                  physical channel is disconnected. */
    
    GsmL1_Status_t status;   ///< Status of the MPH-DISCONNECT-REQ primitive

    uint8_t        u8Tn;     ///< Time-slot [0..7]

} GsmL1_MphDisconnectCnf_t; 

/****************************************************************************
 * Struct : GsmL1_MphActivateReq_t 
 ************************************************************************//**
 *
 * This structure contains all the necessary parameters to activate a 
 * logical channel over a connected physical channel.
 *
 * Primitive : MPH-ACTIVATE-REQ (L3 -> L1)
 * 
 * @ingroup gsml1_api_prim_mph   
 *    
 ****************************************************************************/  
typedef struct GsmL1_MphActivateReq_t
{
    HANDLE             hLayer3;     ///< Layer 3 entity
    HANDLE             hLayer2;     ///< Layer 2 entity
    HANDLE             hLayer1;     /**< Handle of the Layer 1 entity on which the
                                         logical channel is activated */
        
    uint8_t            u8Tn;        ///< Time-slot [0..7]
    GsmL1_SubCh_t      subCh;       ///< Sub-channel number (1Fh if not applicable)
    GsmL1_Sapi_t       sapi;        ///< Service access point identifier
    GsmL1_Dir_t        dir;         ///< Direction path to configure

    float              fBFILevel;   /**< Bad Frame Indication RX power level in dBm 
                                         (use 0 do disable BFI) */
    GsmL1_LogChParam_t logChPrm;    ///< Additional configuration parameters
    
} GsmL1_MphActivateReq_t;

/***************************************************************************
 * Struct : GsmL1_MphActivateCnf
 ************************************************************************//**
 *
 * This structure is sent back to the layer 3 in order to confirm the 
 * reception of a MPH-ACTIVATE-REQ primitive.
 *   
 * @ingroup gsml1_api_prim_mph
 *
 ***************************************************************************/                                 
typedef struct GsmL1_MphActivateCnf
{       
    HANDLE         hLayer3;       /**< The layer 3 handle received from in 
                                       MPH-ACTIVATE-REQ primitive is sent 
                                       back to the layer 3 (unmodified). */
    HANDLE         hLayer1;       /**< Handle of the Layer 1 entity on which the
                                       logical channel is activated */
                                      
    GsmL1_Status_t status;        ///< Status of the MPH-INIT-ACTIVATE-REQ primitive

    uint8_t        u8Tn;          ///< Time-slot [0..7]
    GsmL1_SubCh_t  subCh;         ///< Sub-channel number (1Fh if not applicable)
    GsmL1_Sapi_t   sapi;          ///< Service access point identifier        
    GsmL1_Dir_t    dir;           ///< Uplink/Downlink    

} GsmL1_MphActivateCnf_t; 

/****************************************************************************
 * Struct : GsmL1_MphDeactivateReq_t 
 ************************************************************************//**
 *
 * This structure contains all the necessary parameters to deactivate a 
 * logical channel from a connected physical channel.
 *
 * Primitive : MPH-DEACTIVATE-REQ (L3 -> L1)
 * 
 * @ingroup gsml1_api_prim_mph   
 *    
 ****************************************************************************/  
typedef struct GsmL1_MphDeactivateReq_t
{
    HANDLE        hLayer3;       ///< Layer 3 entity   
    HANDLE        hLayer1;       /**< Handle of the Layer 1 entity on which the
                                      logical channel is activated */
        
    uint8_t       u8Tn;          ///< Time-slot [0..7]
    GsmL1_SubCh_t subCh;         ///< Sub-channel number (1Fh if not applicable)
    GsmL1_Sapi_t  sapi;          ///< Service access point identifier        
    GsmL1_Dir_t   dir;           ///< Uplink/Downlink/Both    

} GsmL1_MphDeactivateReq_t;

/***************************************************************************
 * Struct : GsmL1_MphDeactivateCnf
 ************************************************************************//**
 *
 * This structure is sent back to the layer 3 in order to confirm the 
 * reception of a MPH-DEACTIVATE-REQ primitive.
 *   
 * @ingroup gsml1_api_prim_mph
 *
 ***************************************************************************/                                 
typedef struct GsmL1_MphDeactivateCnf
{       
    HANDLE         hLayer3;       /**< The layer 3 handle received from in 
                                       MPH-DEACTIVATE-REQ primitive is sent 
                                       back to the layer 3 (unmodified). */
    HANDLE         hLayer1;       /**< Handle of the Layer 1 entity on which the
                                      logical channel is activated */

    GsmL1_Status_t status;        ///< Status of the MPH-INIT-DEACTIVATE-REQ primitive
    
    uint8_t        u8Tn;          ///< Time-slot [0..7]
    GsmL1_SubCh_t  subCh;         ///< Sub-channel number (1Fh if not applicable)
    GsmL1_Sapi_t   sapi;          ///< Service access point identifier        
    GsmL1_Dir_t    dir;           ///< Uplink/Downlink    
    
} GsmL1_MphDeactivateCnf_t; 

/****************************************************************************
 * Struct : GsmL1_MphConfigReq_t
 ************************************************************************//**
 *
 * This structure contains all the necessary parameters to configure 
 * physical channel parameters through the SACCH burst.
 *
 * Primitive : MPH-CONFIG-REQ (L3 -> L1)
 * 
 * @ingroup gsml1_api_prim_mph   
 *     
 ****************************************************************************/  
typedef struct GsmL1_MphConfigReq_t
{       
    HANDLE hLayer3;                     ///< Layer 3 instance handle
    HANDLE hLayer1;                     /**< Handle of the Layer 1 entity which has the 
                                             physical channel on which the configuration 
                                             is done. */      
    GsmL1_ConfigParamId_t cfgParamId;   ///< Configuration parameter ID                                         
    GsmL1_ConfigParam_t cfgParams;      ///< Configuration parameters
                                                   
} GsmL1_MphConfigReq_t;

/***************************************************************************
 * Struct : GsmL1_MphConfigCnf
 ************************************************************************//**
 *
 * This structure is sent back to the layer 3 in order to confirm the 
 * reception of a MPH-CONFIG-REQ primitive.
 *   
 * @ingroup gsml1_api_prim_mph
 * 
 ***************************************************************************/                                 
typedef struct GsmL1_MphConfigCnf_t
{       
    GsmL1_Status_t status;              ///< Status of the MPH-CONFIG-REQ primitive

    HANDLE hLayer3;                     ///< Layer 3 instance handle

    HANDLE hLayer1;                     /**< Handle of the Layer 1 entity which has the 
                                             physical channel on which the configuration 
                                             is done. */   
                                               
    GsmL1_ConfigParamId_t cfgParamId;   ///< Configuration parameter ID                                         
    GsmL1_ConfigParam_t cfgParams;      ///< Configuration parameters        
   
} GsmL1_MphConfigCnf_t;  

/****************************************************************************
 * Struct : GsmL1_MphMeasureReq_t
 ************************************************************************//**
 *
 * This structure contains all the necessary parameters to measure the 
 * RSSI on a RF channel.
 *
 * Primitive : MPH-MEASURE-REQ (L3 -> L1)
 * 
 * @ingroup gsml1_api_prim_mph   
 * 
 *    
 ****************************************************************************/  
typedef struct GsmL1_MphMeasureReq_t
{       
    HANDLE hLayer3;                 ///< Layer 3 instance handle
    HANDLE hLayer1;                 /**< Handle of the Layer 1 entity which has the 
                                         physical channel on which the configuration 
                                         is done. */      

	uint32_t u32Duration;           ///< Duration of the measurement (in number of frames, 0:now)
                                                   
} GsmL1_MphMeasureReq_t;

/****************************************************************************
 * Struct : GsmL1_MphMeasureCnf_t
 ************************************************************************//**
 *
 * This structure is sent back to the layer 3 in order to confirm the 
 * reception of a MPH-MEASURE-REQ primitive.
 *
 * Primitive : MPH-MEASURE-CNF (L3 -> L1)
 * 
 * @ingroup gsml1_api_prim_mph   
 * 
 *    
 ****************************************************************************/  
typedef struct GsmL1_MphMeasureCnf_t
{       
    HANDLE hLayer3;                 ///< Layer 3 instance handle
    HANDLE hLayer1;                 /**< Handle of the Layer 1 entity which has the 
                                         physical channel on which the configuration 
                                         is done. */      

	GsmL1_Status_t status;			///< Status of the MPH-MEASURE-REQ primitive
	float fMeanRssi;				///< RF channel mean received signal strength (dBm)
	float fPeakRssi;				///< RF channel maximum received signal strength (dBm)	

	struct
	{
		float fMeanRssi;			///< Timeslot N mean received signal strength (dBm)
		float fPeakRssi;			///< Timeslot N maximum received signal strength (dBm)
	} tn[8];   

} GsmL1_MphMeasureCnf_t;

/***************************************************************************
 * Struct : GsmL1_MphTimeInd
 ************************************************************************//**
 *
 * This structure is sent to the layer 3 at the beginning of every frame.
 * It is sent when u8Tn is equal to 0.
 *  
 * @ingroup gsml1_api_prim_mph
 *
 ***************************************************************************/                                 
typedef struct GsmL1_MphTimeInd_t
{       
    HANDLE   hLayer1; /**< Handle of the Layer 1 entity sending the 
                           MphTimeInd primitive */
    uint32_t u32Fn;   ///< Current frame number [0 .. 2 715 647]
    uint8_t  u8Tn;    ///< Current timeslot [0..7]        

} GsmL1_MphTimeInd_t;

/****************************************************************************
 * Struct : GsmL1_MphSyncInd_t
 ************************************************************************//**
 *
 * This structure is sent back to the layer 3 in order indicate when the
 * RF device is synchronized to a surrounding BTS or also when it looses its
 * synchronization.
 *
 * Primitive : MPH-SYNC-IND (L1 -> L3)
 * 
 * @ingroup gsml1_api_prim_mph   
 * 
 *    
 ****************************************************************************/ 
typedef struct GsmL1_MphSyncInd_t
{
    HANDLE hLayer1;     ///< Handle of the device which is sync/unsynced
    uint8_t u8Synced;   ///< Indicates if the RF device is synchronized to a BTS.

} GsmL1_MphSyncInd_t;

/******************************************************************************
 * Struct : GsmL1_PhDataInd_t
 **************************************************************************//**
 *   
 * This structure is sent to the layer 2 to pass message units containing
 * frames used by the data link layer or RLC/MAC peer-to-peer communications.
 *
 * @ingroup gsml1_api_prim_ph
 * 
 *****************************************************************************/
typedef struct GsmL1_PhDataInd_t
{  
    HANDLE                hLayer2;      /**< Layer 2 instance handle.
    
                                             This handle is sent through the 
                                             MPH-CONNECT-REQ primitive. */

    HANDLE                hLayer1;      /**< Handle of the Layer 1 entity on which the
                                             logical channel is activated */
     
    GsmL1_Sapi_t          sapi;         ///< Service access point identifier        
    GsmL1_Dir_t           dir;          ///< Uplink/Downlink    

    GsmL1_MeasParam_t     measParam;    ///< Measurement parameters infos   
    GsmL1_MsgUnitParam_t  msgUnitParam; ///< Message unit sent from the MS

    uint16_t              u16Arfcn;     ///< Absolute radio frequency number    
    uint8_t               u8Tn;         ///< Time-slot [0..7]
    GsmL1_SubCh_t         subCh;        ///< Sub-channel number (1Fh if not applicable)      
    uint32_t              u32Fn;        ///< Frame number of the 1st RX burst
    uint8_t               u8BlockNbr;   ///< Block number

} GsmL1_PhDataInd_t;

/******************************************************************************
 * Struct : GsmL1_PhConnectInd_t
 **************************************************************************//**
 *
 * This structure is sent to the layer 2 in order to indicate that a 
 * specific type of logical channel has been connected on a physical channel.
 *   
 * @ingroup gsml1_api_prim_ph
 *
 *****************************************************************************/
typedef struct GsmL1_PhConnectInd_t
{
    HANDLE        hLayer2;      /**< Layer 2 instance handle.    
                                     This handle is sent in the MPH-ACTIVATE-REQ 
                                     primitive. */

    HANDLE        hLayer1;      /**< Handle of the Layer 1 entity on which the
                                     logical channel is activated */
                                           
    uint16_t      u16Arfcn;     ///< Absolute radio frequency number    
    uint8_t       u8Tn;         ///< Time-slot [0..7]
    GsmL1_SubCh_t subCh;        ///< Sub-channel number (1Fh if not applicable)
    GsmL1_Sapi_t  sapi;         ///< Service access point identifier
    GsmL1_Dir_t   dir;          ///< Indicate the path of the transmission.

} GsmL1_PhConnectInd_t;

/******************************************************************************
 * Struct : GsmL1_PhReadyToSendInd_t
 **************************************************************************//**
 *  
 * This structure is sent to allow the data link or the RLC/MAC layer 
 * to synchronize to the next instant of physical transmission. 
 *
 * @ingroup gsml1_api_prim_ph
 *
 *****************************************************************************/
typedef struct GsmL1_PhReadyToSendInd_t
{
    HANDLE              hLayer1;    /**< Handle of the Layer 1 entity on which the
                                         logical channel is activated */
                                               
    HANDLE              hLayer2;    /**< Layer 2 instance handle.
    
                                         This handle is sent in the MPH-ACTIVATE-REQ 
                                         primitive. */

    GsmL1_Sapi_t        sapi;       ///< Service access point identifier

    uint32_t            u32Fn;      ///< Frame number of the 1st TX burst
    
    uint16_t            u16Arfcn;   ///< Absolute radio frequency number    
    uint8_t             u8Tn;       ///< Timeslot number [0..7]
    GsmL1_SubCh_t       subCh;      ///< Sub-channel number (1Fh if not applicable)

    uint8_t             u8BlockNbr; ///< Block number      
                  
} GsmL1_PhReadyToSendInd_t;

/******************************************************************************
 * Struct : GsmL1_PhEmptyFrameReq_t
 **************************************************************************//**
 * 
 * This structure is sent back to the layer 1 when no frame has to be 
 * transmitted on the reception of a PH-READY-TO-SEND-IND primitive.
 *   
 * @ingroup gsml1_api_prim_ph
 *
 *****************************************************************************/
typedef struct GsmL1_PhEmptyFrameReq_t
{
    HANDLE              hLayer1;    /**< Handle of the Layer 1 entity on which the
                                         logical channel is activated */

    uint32_t            u32Fn;      ///< Frame number of the 1st TX burst
    
    uint8_t             u8Tn;       ///< Timeslot number
    GsmL1_SubCh_t       subCh;      ///<Sub-channel number (1Fh if not applicable)
    
    GsmL1_Sapi_t        sapi;       ///< Service access point identifier    

    uint8_t             u8BlockNbr; ///< Block number
            
} GsmL1_PhEmptyFrameReq_t;

/******************************************************************************
 * Struct : GsmL1_PhDataReq_t
 **************************************************************************//**
 *  
 * This structure is used to send a message unit to the layer 1 in response
 * to a PH-READY-To-SEND-IND primitive.
 *
 * @note
 *   For FR and HR traffic channel (TCH/FS, TCH/HS), the first byte of the 
 *   message unit represents the payload type of the data (GsmL1_FrPlType_t 
 *   or GsmL1_HrPlType_t). It is follow by the paqcked bits output from the 
 *   vocoder.
 *
 *   For AMR traffic channel (TCH/AFS, TCH/AHS), the first byte of the message 
 *   unit represents the payload type of the data (GsmL1_FrPlType_t or 
 *   GsmL1_HrPlType_t). The second byte is always the codec mode indication 
 *   (CMI) and the third byte is always the codec mode response (CMR). They 
 *   are followed by the IF2 frame.
 *
 *   Refer to 3GPP TS 45.009 for more information about the in-band signaling.
 *   The format of IF2 frames are defined in 3GPP TS 26.101. 
 *    
 * @see GsmL1_FrPlType_t 
 * @see GsmL1_HrPlType_t
 * @see GsmL1_CodecMode_t
 *
 * @ingroup gsml1_api_prim_ph
 *
 *****************************************************************************/
typedef struct GsmL1_PhDataReq_t
{
    HANDLE                hLayer1;      /**< Handle of the Layer 1 entity on which the
                                             logical channel is activated */
                                               
    uint32_t              u32Fn;        ///< Frame number of the 1st TX burst    

    GsmL1_Sapi_t          sapi;         ///< Service access point identifier        
    GsmL1_MsgUnitParam_t  msgUnitParam; ///< Message unit structure
    
    uint8_t               u8Tn;         ///< Timeslot number
    GsmL1_SubCh_t         subCh;        ///< Sub-channel number (1Fh if not applicable)

    uint8_t               u8BlockNbr;   ///< Block number    
    
} GsmL1_PhDataReq_t;

/***************************************************************************
 * Struct : GsmL1_PhRaInd_t
 ************************************************************************//**
 *
 * This structure is sent to the layer 3 on the arrival (in the network )
 * of a RACH frame.
 *  
 * @ingroup gsml1_api_prim_ph
 *
 ***************************************************************************/ 
typedef struct GsmL1_PhRaInd_t
{
    HANDLE                hLayer1;      /**< Handle of the Layer 1 entity on which the
                                             logical channel is activated */
    HANDLE                hLayer2;      ///< Layer 2 instance handle.

    uint32_t              u32Fn;        ///< Frame number of the RX burst

    uint16_t              u16Arfcn;     ///< Absolute radio frequency number    
    uint8_t               u8Tn;         ///< Timeslot number
    uint8_t               u8BlockNbr;   ///< Block number

    GsmL1_Sapi_t          sapi;         ///< Service access point identifier 

    GsmL1_MsgUnitParam_t  msgUnitParam; ///< Message unit structure
    GsmL1_MeasParam_t     measParam;    ///< Measurement parameters infos

    GsmL1_BurstType_t     burstType;    /**< Indicate which training sequence was used 
                                             to send the RACH/PRACH burst */
    GsmL1_SubCh_t         subCh;        ///< Sub-channel number (1Fh if not applicable)                                               

} GsmL1_PhRaInd_t;

/***************************************************************************
 * Struct : GsmL1_Prim_t
 ************************************************************************//**
 *
 * Layer 1 primitive definition. 
 *  
 * @ingroup gsml1_api_prim
 *
 ***************************************************************************/ 
typedef struct
{
    GsmL1_PrimId_t id;                              ///< Primitive ID
    union
    {    
        GsmL1_MphInitReq_t       mphInitReq;        ///< Initialize the base station. 
        GsmL1_MphCloseReq_t      mphCloseReq;       ///< Closes the base station. 
        GsmL1_MphConnectReq_t    mphConnectReq;     ///< Connect a physical channel. 
        GsmL1_MphDisconnectReq_t mphDisconnectReq;  ///< Disconnect a physical channel. 
        GsmL1_MphActivateReq_t   mphActivateReq;    ///< Activate a logical channel over a physical channel. 
        GsmL1_MphDeactivateReq_t mphDeactivateReq;  ///< Deactivate a logical channel from a physical channel. 
        GsmL1_MphConfigReq_t     mphConfigReq;      ///< Configure a physical channel. 
        GsmL1_MphMeasureReq_t    mphMeasureReq;     ///< Measure the RSSI and frequency drift
        GsmL1_MphInitCnf_t       mphInitCnf;        ///< Confirm the reception of a MPH-INIT_REQ primitive. 
        GsmL1_MphCloseCnf_t      mphCloseCnf;       ///< Confirm the reception of a MPH-CLOSE_REQ primitive. 
        GsmL1_MphConnectCnf_t    mphConnectCnf;     ///< Returns an handle pointing to the connected physical channel. 
        GsmL1_MphDisconnectCnf_t mphDisconnectCnf;  ///< Confirm the reception of a MPH-DISCONNECT_REQ primitive. 
        GsmL1_MphActivateCnf_t   mphActivateCnf;    ///< Confirm the reception of a MPH-ACTIVATE_REQ primitive. 
        GsmL1_MphDeactivateCnf_t mphDeactivateCnf;  ///< Confirm the reception of a MPH-DEACTIVATE_REQ primitive. 
        GsmL1_MphConfigCnf_t     mphConfigCnf;      ///< Confirm the reception of a MPH-CONFIG_REQ primitive. 
        GsmL1_MphMeasureCnf_t    mphMeasureCnf;     ///< Return the RSSI and freq. drift
        GsmL1_MphTimeInd_t       mphTimeInd;        ///< Infom upper layer of the actual timeslot and frame number. 
        GsmL1_MphSyncInd_t       mphSyncInd;        ///< Synchronization status indication
        GsmL1_PhEmptyFrameReq_t  phEmptyFrameReq;   ///< Layer 2 has nothing to transmit on a PH-READY-TO-SEND. 
        GsmL1_PhDataReq_t        phDataReq;         ///< Transfer a message frame unit to the layer 1. 
        GsmL1_PhConnectInd_t     phConnectInd;      ///< Inform layer 2 that a logical channel has connected on layer 1. 
        GsmL1_PhReadyToSendInd_t phReadyToSendInd;  ///< Ask layer 3 for something to transmit. 
        GsmL1_PhDataInd_t        phDataInd;         ///< Transmit a message frame unit to layer 2. 
        GsmL1_PhRaInd_t          phRaInd;           ///< Inform the layer 2 of a random access initiated by the MS.
    } u;
} GsmL1_Prim_t;


/****************************************************************************
 *                               Public Functions                           * 
 ****************************************************************************/

#ifdef _TMS320C6400

/****************************************************************************
 *                             Public routines                              *
 ****************************************************************************/

/****************************************************************************
 * Function : GsmL1_SendMphReq
 ************************************************************************//**
 *
 * This routine is used by the layer 3 to request a service from layer 1.  
 *    
 * @param [in] pMphReq 
 *    Structure containing the primitive parameters
 *       
 * @return
 *    GsmL1_Status_Success or the error code
 *
 * @ingroup gsml1_api_prim_mph
 *
 ****************************************************************************/
GsmL1_Status_t GsmL1_SendMphReq( GsmL1_Prim_t *pMphReq );

/****************************************************************************
 * Function : GsmL1_SendMphCnf
 ************************************************************************//**
 *
 * This routine is used by layer 1 to send the confirmation of a previously
 * received request.
 *   
 * @param [out] pMphCnf 
 *    Structure containing the confirmation primitive parameters
 *       
 * @return
 *    GsmL1_Status_Success or the error code
 *  
 * @ingroup gsml1_api_prim_mph
 *
****************************************************************************/                               
GsmL1_Status_t GsmL1_SendMphCnf( GsmL1_Prim_t *pMphCnf );

/****************************************************************************
 * Function : GsmL1_SendMphInd
 ************************************************************************//**
 *
 * This routine is used by layer 1 to indicate something to layer 3.
 *
 * @param [out] pMphInd 
 *    Structure containing indication primitive parameters
 *       
 * @return
 *    GsmL1_Status_Success or the error code
 *
 * @ingroup gsml1_api_prim_mph
 *
****************************************************************************/
GsmL1_Status_t GsmL1_SendMphInd( GsmL1_Prim_t *pMphInd );

/****************************************************************************
 * Function : GsmL1_SendPhReq
 ************************************************************************//**
 *
 * This routine is used by the layer 2 to request a service from layer 1.  
 *   
 * @param [in] pPhReq 
 *    Structure containing request primitive parameters
 *       
 * @return
 *    GsmL1_Status_Success or the error code
 *  
 * @ingroup gsml1_api_prim_ph
 *
****************************************************************************/    
GsmL1_Status_t GsmL1_SendPhReq( GsmL1_Prim_t *pPhReq );

/****************************************************************************
 * Function : GsmL1_SendPhCnf
 ************************************************************************//**
 *
 * This routine is used by layer 1 to send the confirmation of a previously
 * received request from layer 2.
 *   
 * @param [out] pPhCnf 
 *    Structure containing the confirmation primitive parameters
 *       
 * @return
 *    GsmL1_Status_Success or the error code
 *  
 * @ingroup gsml1_api_prim_mph
 *
****************************************************************************/                               
GsmL1_Status_t GsmL1_SendPhCnf( GsmL1_Prim_t *pPhCnf );

/****************************************************************************
 * Function : GsmL1_SendPhInd
 ************************************************************************//**
 *
 * This routine is used by layer 1 to indicate something to layer 2.
 *
 * @param [out] pPhInd 
 *    Structure containing indication primitive parameters
 *       
 * @return
 *    GsmL1_Status_Success or the error code
 *
 * @ingroup gsml1_api_prim_mph
 *
****************************************************************************/
GsmL1_Status_t GsmL1_SendPhInd( GsmL1_Prim_t *pPhInd );

#endif  // _TMS320C6400

#ifdef __cplusplus
}
#endif  // extern "C"

#endif	// GSML1PRIM_H__
